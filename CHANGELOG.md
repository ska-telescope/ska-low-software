# Version History

## unreleased

### Fixed
* With ska-tmc-cspleafnodes 0.23.7, TMC now produces delay polynomials that account for the known relative fixed delays between stations. The fix has been validated by analysing visibility data which show much smaller residual delays. See [SKB-798](https://jira.skatelescope.org/browse/SKB-798).


### Tickets
* [LOW-1024](https://jira.skatelescope.org/browse/LOW-1024): Support configuring for fewer beams
* [LOW-1023](https://jira.skatelescope.org/browse/LOW-1023): Enable read-only access to the EDA from Jupyterhub
* [LOW-1022](https://jira.skatelescope.org/browse/LOW-1022): Enable per-station archivers for spshw and pasd attributes
* [LOW-1017](https://jira.skatelescope.org/browse/LOW-1017): Support deploying a custom runner, and deploy in AA0.5
* [SKB-494](https://jira.skatelescope.org/browse/SKB-494): Update MCCS versions to support new SPEAD format
* [LOW-1008](https://jira.skatelescope.org/browse/LOW-1008): Deploy s9-2
* [LOW-988](https://jira.skatelescope.org/browse/LOW-988): Pipeline support for publishing chart
* [LOW-962](https://jira.skatelescope.org/browse/LOW-962): Deploy from telmodel
* [LOW-973](https://jira.skatelescope.org/browse/LOW-973): Update MCCS for DAQ and pointing fixes
* [LOW-960](https://jira.skatelescope.org/browse/LOW-960): Various subsystem and grafana dashboard updates
* [SPRTS-224](https://jira.skatelescope.org/browse/SPRTS-224): Bump DAQ to a version that fixes X/Y labeling
* [LOW-937](https://jira.skatelescope.org/browse/LOW-937): Update Jupyterhub and MCCS in all environments
* [LOW-890](https://jira.skatelescope.org/browse/LOW-890): Update to latest ska-low-mccs
* [LOW-917](https://jira.skatelescope.org/browse/LOW-917): Two DAQs per station
* [LOW-879](https://jira.skatelescope.org/browse/LOW-879): Update Low ITF device names to adhere to MCCS conventions
* [MCCS-2120](https://jira.skatelescope.org/browse/MCCS-2120): Support DAQ data sync
* [LOW-856](https://jira.skatelescope.org/browse/LOW-856): Merge ska-low-south and ska-low-north into a single ska-low chart
* [LOW-850](https://jira.skatelescope.org/browse/LOW-850): Upgrade to development CBF versions supporting automatic inter-device subscriptions configured at deploy-time
* [LOW-847](https://jira.skatelescope.org/browse/LOW-847): Automatically check out ska-low-jupyter in JupyterHub environments, not ska-ost-low-aavs3
* [LOW-831](https://jira.skatelescope.org/browse/LOW-831): Add automatic provisioning of EDA attribute configuration via labelled ConfigMaps
* [LOW-830](https://jira.skatelescope.org/browse/LOW-830): Add Grafana dashboards for generic SKA Tango devices, CBF connector, SPS and PaSD 
* [LOW-830](https://jira.skatelescope.org/browse/LOW-830): Enable automatic provisioning of Grafana dashboards via labelled ConfigMaps
* [LOW-830](https://jira.skatelescope.org/browse/LOW-830): Grafana is now deployed with an EDA datasource configured
* [PERENTIE-2404](https://jira.skatelescope.org/browse/PERENTIE-2404): Fix ARP issues in CBF by upgrading to ska-low-cbf-conn 0.5.4
* [LOW-828](https://jira.skatelescope.org/browse/LOW-828): Add SKA Portal to ska-low-skaffold
* [LOW-840](https://jira.skatelescope.org/browse/LOW-840): Change AA0.5 SPC deployment namespace from `sdp` to `ska-low`
* [LOW-840](https://jira.skatelescope.org/browse/LOW-840): Add deployment for AA0.5 MCCS cluster at Inyarrimanha Ilgari Bundara
* [LOW-825](https://jira.skatelescope.org/browse/LOW-825): Add ska-low-data-sync chart, factored out from ska-low-south and now used to sync captured data from both DAQ and CNIC to central volumes
* [LOW-816](https://jira.skatelescope.org/browse/LOW-816): Add deployment for AAVS3
* [LOW-816](https://jira.skatelescope.org/browse/LOW-816): Add PVCs for DAQ and EEPs to ska-low-north
* [Low-796](https://jira.skatelescope.org/browse/Low-796): Add deployment for Low ITF across two namespaces, one for ska-low-south and one for ska-low-north
* [LOW-796](https://jira.skatelescope.org/browse/LOW-796): Add ska-low-north chart
* [LOW-796](https://jira.skatelescope.org/browse/LOW-796): Refactor helmfile to allow values for multiple environments
* [LOW-795](https://jira.skatelescope.org/browse/LOW-795): Add deployment for AA0.5 cluster at Pawsey
* [LOW-771](https://jira.skatelescope.org/browse/LOW-771): Add ska-low-south chart containing all telescope software downstream of MCCS
* [LOW-771](https://jira.skatelescope.org/browse/LOW-771): Add ska-low-skaffold chart containing Tango and Tango tooling
* [LOW-675](https://jira.skatelescope.org/browse/LOW-675): Add README.md describing the proposed charts
* [LOW-774](https://jira.skatelescope.org/browse/LOW-774): Bootstrap repo with license, docs and changelog
