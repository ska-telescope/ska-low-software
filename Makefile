include .make/base.mk
include .make/helm.mk
include .make/oci.mk

HELM_CHARTS_TO_PUBLISH = ska-low

vault-login:
	vault token lookup -address https://vault.skao.int \
	|| vault login -address https://vault.skao.int -method=oidc
