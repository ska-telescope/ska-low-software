# SKA-Low software

This repository contains the Helm charts and Helmfile configuration for deploying the integrated SKA-Low telescope software. It is aimed at deploying to facilities with real telescope hardware available, and maintained by Team Vulcan.

These facilities are currently deployed from this repository:
* The production SKA-Low data processing cluster in the SPC (science processing centre) at the Pawsey Supercomputing Centre in Perth
* The production SKA-Low MCCS cluster at the CPF (central processing facility) at Inyarrimanha Ilgari Bundara, our Murchison Radio-astronomy Observatory
* The model telescope at the Low ITF (integration and testing facility) in Geraldton
* AAVS3 (the Aperture Array Verification System), also at Inyarrimanha Ilgari Bundara

# Overview

## Helm charts

ska-low-software separates the deployment of Tango, and its related services and interfaces, from the telescope software itself.

The **ska-low-skaffold** chart deploys the Tango database, UIs (Grafana, JupyterHub and Taranta), and the EDA (minus its TimescaleDB backend), plus some tools like our configuration exporter and skuid. These are configured to be integrated with one another in useful ways out of the box. Skaffold gives you everything a successful SKA-Low deployment needs, minus the actual telescope software components.

The **ska-low** chart contains the telescope components. This chart uses Helm tags to split its dependencies into two categories, which map to the two clusters comprising the SKA-Low production infrastructure.
* the _north_ tag deploys the "upstream" telescope components: MCCS, PaSD, and TANGO devices for managing power and infrastructure devices like PDUs, cabinets, UPSs, etc.
* the _south_ tag deploys the "downstream" telescope components: CSP (including CBF and eventually PST) and SDP, and the global telescope control layer in the form of TMC and OET.

An additional chart, **ska-low-data-sync**, is a utility chart used by ska-low to synchronise data captured locally by telescope components (DAQ and CNIC) to a central shared Ceph volume.

## Helmfile configuration

Configuration for different facilities is handled with Helmfile.

The `helmfile.d` directory contains a helmfile.yaml, and a directory of values for each environment defined in helmfile.yaml. Each environment may configure a `north` deployment and a `south` deployment. Each deployment comprises a release of ska-low-skaffold, and a release of the north or south chart. Deployments must specify at least a `kubeContext` and a `namespace`, and may optionally also specify a `releaseName` and a `skaffoldReleaseName`.

# Features

## JupyterHub set up for interactive testing and experimentation
* many science, astronomy, and SKA-specific packages pre-installed
* ITango installed and TANGO_HOST pre-populated
* a persistent home directory for each user
* an automatic checkout of a configurable repository
* jupyterlab-git integrated with an SSH keypair generated for each user
* jupyterlab-execute-time installed and cell time tracking enabled by default
* shared scratch space and telescope data product volumes mounted
* k8s service account credentials mounted for reading from the cluster

## EDA auto-configuration from ConfigMaps
* EDA is configured to look for and apply yaml2archiving configuration files in labelled ConfigMaps
* uses a fork of yaml2archiving with improved performance and idempotence
* EDA configurations bundled with north and south charts (please contribute!)

## Grafana integrated with EDA and configured for dashboard discovery
* Grafana is deployed with an EDA datasource preconfigured
* Dashboards are discovered in labelled ConfigMaps and automatically added
* Grafana dashboards bundled with north and south charts (please contribute!)

# TODO

- start publishing charts and make first release
- pin different versions to different environments
- document all platform requirements and required Helm values
- define a versioning policy (when will major versions be bumped?)
- implement deployment tracking (git tags?)
- release version 1.0

# Chart details

Below are listed the three charts, and the rationale behind each of their dependencies.

## ska-low-skaffold

### TANGO layer
* **ska-tango-base** provides the Tango database and an ITango debugging pod.
* **ska-tango-archiver** provides HDB++ archiver Tango devices, plus web-based tools to configure and inspect the archiving system.

### UIs
* **grafana** provides visual time-series dashboard for TANGO attributes archived by EDA.
* **jupyterhub** provides a web-based Python environment for interacting with the telescope. Tests and experiments are performed from here. Features of the JupyterHub environment provided by ska-low-jupyter:
  * science and TANGO dependencies pre-installed, TANGO_HOST pre-populated
  * a persistent home directory for each user
  * access to a shared data volume for storage of telescope data products and data such as CNIC captures and EEPs
  * jupyterlab-git integrated, jupyterlab-execute-time extension installed and cell time tracking enabled by default
* **ska-tango-tangogql** and **ska-tango-taranta** together form the front-end of Taranta, a web-based UI for interacting with TANGO devices.

### Utilities
* **ska-ser-skuid** is a global UID service for SKA.
* **ska-k8s-config-exporter** is a tool to capture the state of a TANGO deployment, both in terms of TANGO device properties and Kubernetes resources.
* **ska-tango-util** is included in the chart dependencies, but is not used directly by the chart. It's included in order to "pin" the version of ska-tango-util used by other dependencies.

## ska-low

### MCCS
* **ska-low-mccs** deploys the high-level control devices from the ska-low-mccs repository, and also includes ska-low-mccs-pasd and ska-low-mccs-spshw as dependencies for control of the PaSD and SPS hardware respectively.

### CSP
* **ska-low-csp** contains the TANGO devices for control and monitoring of the CBF and PST, and a higher-level CSP LMC control layer.
* **ska-low-cbf-tango-cnic** deploys CNIC devices - personalities for the Alveo FPGAs that can capture, replay and generate CBF traffic.

### SDP
* **ska-sdp** deploys the components that constitude the SKA SDP (science data processor).

### TMC
* **ska-tmc-low** deploys TMC (telescope monitoring and control), the highest-level TANGO interface for the telescope.

* **ska-tango-util** is once again included to avoid a ska-tango-util version from one dependency overwriting another. This implies that all dependencies are forward-compatible with the version we include here.


# Installation

After cloning the repository, you can then install the python dependencies with the following command:

```bash
poetry install
```

or

```
pip install .
```

You will then need to install the following dependencies on your system:

- [helm](https://helm.sh/docs/intro/install/)
- [helmfile](https://helmfile.readthedocs.io/en/latest/#installation)
- [vault](https://developer.hashicorp.com/vault/install)

# Developing and deploying

The three most important `helmfile` commands are `helmfile deps` to update chart dependencies, `helmfile diff` to compare your changes with what is currently deployed and `helmfile sync` to deploy your changes to the cluster.

## `helmfile deps`

If there have been any changes to the helm charts versions (`charts/ska-low/Chart.yaml`) (by you or others) then you will need to run the following command:

```bash
helmfile deps
```

Running this command is time consuming so try to only run it when necessary and use `--skip-deps` to prevent it being run in addition to the following commands.
If you see different pod image versions than you expect when doing a `helm diff`, you man need to run `helmfile deps` again.

## `helmfile diff`

The `helmfile diff` command can be used to see the changes that will be made to the kubernetes cluster when the helmfile is applied.

```
helmfile --skip-deps -e <environment> -l where=<south or north>,role=<apps or skaffold> diff --context 2
```

Where `-e <environment>` is the environment you are deploying to (e.g. `aa0.5`, `low-itf` or `aavs3`),
`-l where=<south or north>` is the location you are deploying to (e.g. south, north) and
`-l role=<apps or skaffold>` is the role of the deployment (e.g. apps, skaffold).
We also use `--skip-deps` to avoid having to redownload the dependencies and
`--context 2` to give two lines of context above and below any changes (sometimes more is useful).

There are some things that will change after every sync like the secret values which can be ignored.
If diff shows you the changes you expected you can move onto the next command.

## `helmfile sync`

Once you are ready to deploy your changes you can run the following command:

```
helmfile --skip-deps -e <environment> -l where=<south or north>,role=<apps or skaffold> sync
```

If everything works then it's time to make a MR.
