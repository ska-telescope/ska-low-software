-- Create convenience views that join att_conf to each of the attribute tables and
-- drop the "att_" prefix for names like "scalar_devstate", "image_devdouble" etc
CREATE FUNCTION pg_temp.create_eda_view(table_name text)
   RETURNS void
AS
$$
BEGIN
  EXECUTE format(
    'create or replace view %I as select * from %I natural join att_conf',
    right(table_name, -4),
    table_name
  );
END
$$
LANGUAGE plpgsql;

SELECT pg_temp.create_eda_view(table_name)
FROM information_schema.tables
WHERE table_name ~ 'att_(scalar|image|array)_.+';
