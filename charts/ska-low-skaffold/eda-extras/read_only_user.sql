DO
$do$
BEGIN
   IF NOT EXISTS (SELECT FROM pg_catalog.pg_roles WHERE rolname = 'eda_ro') THEN
      CREATE USER eda_ro WITH PASSWORD '{{ .Values.global.eda.viewer_password }}';
   END IF;
END
$do$;

-- TimescaleDB has long-running compression jobs, which acquire a lock that
-- blocks this command. And this command apparently acquires locks that block
-- general reads, which means that nothing in Grafana works. This only needs
-- to be run once whenever a table is added, so just comment it out for now
-- so that deploying skaffold doesn't break the EDA for everyone.
-- GRANT SELECT ON ALL TABLES IN SCHEMA public TO eda_ro;
