CREATE TABLE IF NOT EXISTS "antenna_locations" (
  id TEXT NOT NULL UNIQUE,
  station TEXT NOT NULL,
  eep SMALLINT NOT NULL,
  latitude DOUBLE PRECISION NOT NULL,
  longitude DOUBLE PRECISION NOT NULL,
  east DOUBLE PRECISION NOT NULL,
  north DOUBLE PRECISION NOT NULL,
  up DOUBLE PRECISION NOT NULL,
  label TEXT NOT NULL,
  smartbox TEXT NOT NULL,
  smartbox_port SMALLINT NOT NULL,
  tpm TEXT NOT NULL,
  tpm_fibre_input SMALLINT NOT NULL,
  tpm_x_channel SMALLINT NOT NULL,
  tpm_y_channel SMALLINT NOT NULL,
  masked BOOLEAN NOT NULL,
  PRIMARY KEY (station, label)
);

COMMENT ON TABLE "antenna_locations" is 'Antenna Locations';

{{- range $station_name, $station_data := .Values.platform.stations }}

{{- $table_value := list}}
{{- range $label, $data := $station_data.antennas }}
{{- with $data}}
{{- $row_string := printf "('%s-%d', '%s', %d, %f, %f, %f, %f, %f, '%s', '%s', %d, '%s', %d, %d, %d, %t)" $station_name (int .eep) $station_name (int .eep) .position.latitude .position.longitude .location_offset.east .location_offset.north .location_offset.up $label .smartbox (int .smartbox_port) .tpm (int .tpm_fibre_input) (int .tpm_x_channel) (int .tpm_y_channel) (.masked | default false)}}
{{- $table_value = append $table_value $row_string }}
{{- end}}
{{- end}}

INSERT INTO "antenna_locations" (id, station, eep, latitude, longitude, east, north, up, label, smartbox, smartbox_port, tpm, tpm_fibre_input, tpm_x_channel, tpm_y_channel, masked)
VALUES
{{ $table_value | join ", "}}
ON CONFLICT (station, label) DO UPDATE set
  latitude = excluded.latitude,
  longitude = excluded.longitude,
  east = excluded.east,
  north = excluded.north,
  up = excluded.up,
  smartbox = excluded.smartbox,
  smartbox_port = excluded.smartbox_port,
  tpm = excluded.tpm,
  tpm_fibre_input = excluded.tpm_fibre_input,
  tpm_x_channel = excluded.tpm_x_channel,
  tpm_y_channel = excluded.tpm_y_channel,
  masked = excluded.masked;
{{- end}}
