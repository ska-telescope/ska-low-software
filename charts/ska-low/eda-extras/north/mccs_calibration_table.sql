-- Retrieved from https://gitlab.com/ska-telescope/mccs/ska-low-mccs/-/blob/e69abf3c1f589e06adc1dc28e81b9336cc1d7a40/charts/ska-low-mccs/data/create_tables.sql

CREATE TABLE IF NOT EXISTS tab_mccs_calib (
    id serial NOT NULL PRIMARY KEY,
    creation_time timestamp NOT NULL,
    outside_temperature DOUBLE PRECISION NOT NULL,
    frequency_channel smallint NOT NULL,
    station_id smallint NOT NULL,
    preferred boolean NOT NULL DEFAULT FALSE,
    calibration_path text NOT NULL
);
