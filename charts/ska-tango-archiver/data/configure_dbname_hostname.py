import getopt, sys
import tango
from tango import Database
from tango import ConnectionFailed, CommunicationFailed, DevFailed
from tango import DeviceProxy, DevFailed, AttributeProxy
import time
import json

ARCHIVER_HOST = ""
ARCHIVER_DBNAME = ""
ARCHIVER_PORT = ""
ARCHIVER_DBUSER = ""
ARCHIVER_DBPASSWORD = ""
tango_host = ""

try:
    opts, args = getopt.getopt(sys.argv[1:], "a:h:d:P:u:P:", ["attrfile=", "hostname=", "dbname=", "port=", "dbuser=", "dbpassword=" ])

except getopt.GetoptError:
    print("Please provide proper arguments.")
    print("Usage: $python lib_configuration.py --attrfile=<filepath> --hostname=<hostname> --dbname=<dbname> --port=<port> --dbuser=<dbuser> --dbpassword=<dbpassword>")
    print("       filepath: File containing configuration of attributes to archive")
    print("       hostname: Host name of the archiver database server")
    print("       dbname: Archiver database name")
    print("       port: Archiver database server port")
    print("       dbuser: Archiver database server user")
    print("       dbpassword: Archiver database server password")
    sys.exit(2)

for opt, arg in opts:
    if opt in ("-a", "--attrfile"):
        ATTR_LIST_FILE = arg
    if opt in ("-h", "--hostname"):
        ARCHIVER_HOST = arg
    elif opt in ("-d", "--dbname"):
        ARCHIVER_DBNAME = arg
    elif opt in ("-P", "--port"):
        ARCHIVER_PORT = arg
    elif opt in ("-u", "--dbuser"):
        ARCHIVER_DBUSER = arg
    elif opt in ("-p", "--dbpassword"):
        ARCHIVER_DBPASSWORD = arg

try:

    conf_manager_device_list = []
    evt_subscriber_device_list = []
    conf_manager_proxy_dict = {}
    evt_subscriber_proxy_dict = {}

    with open(ATTR_LIST_FILE, "r") as attrib_list_file:
        archiver_json = json.load(attrib_list_file)
        ARCHIVER_CONFIG = archiver_json["archiver"]
        for cm in ARCHIVER_CONFIG:
            conf_manager = cm["confmanager"]
            conf_manager_device_list.append(conf_manager)
            configurations = cm["configuration"]
            for es in configurations:
                event_subscriber = es["eventsubscriber"]
                evt_subscriber_device_list.append(event_subscriber)
        for conf_manager in conf_manager_device_list:
            conf_manager_proxy_dict[conf_manager] = DeviceProxy(conf_manager)
        for evt_subscriber in evt_subscriber_device_list:
            evt_subscriber_proxy_dict[evt_subscriber] = DeviceProxy(evt_subscriber)

        DB_NAME = Database()
        hostname = ARCHIVER_HOST 
        libname = "libhdb++timescale.so"
        dbname = ARCHIVER_DBNAME
        port = ARCHIVER_PORT
        dbuser = ARCHIVER_DBUSER
        dbpassword = ARCHIVER_DBPASSWORD

        host_name = "host={}".format(hostname)
        lib_name = "libname={}".format(libname)
        db_name = "dbname={}".format(dbname)
        port_number = "port={}".format(port)
        user_name = "user={}".format(dbuser)
        user_password = "password={}".format(dbpassword)
        connect_string= "connect_string= \n"+host_name+"\n"+db_name+"\n"+port_number+"\n"+user_name+"\n"+user_password
        
        prop = {"LibConfiguration": [connect_string,host_name, lib_name, db_name, port_number, user_name, user_password]}
        for conf_manager in conf_manager_device_list:
            DB_NAME.put_device_property(conf_manager, prop)
            conf_manager_proxy_dict[conf_manager].put_property(prop)
            print("Updated hostname and dbname in LibConfiguration property of CM :{cm_device}".format(cm_device = conf_manager_proxy_dict[conf_manager]))
            conf_manager = conf_manager.split("/")
            cm_admin_proxy = DeviceProxy("dserver/hdbppcm-srv/" + conf_manager[len(conf_manager)-1][-2:])
            print("Restarting device :{device_name}".format(device_name = cm_admin_proxy))
            cm_admin_proxy.RestartServer()
        
        for evt_subscriber in evt_subscriber_device_list:
            DB_NAME.put_device_property(evt_subscriber, prop)
            evt_subscriber_proxy_dict[evt_subscriber].put_property(prop)
            print("Updated hostname and dbname in LibConfiguration property of ES :{es_device}".format(es_device = evt_subscriber_proxy_dict[evt_subscriber]))
            evt_subscriber = evt_subscriber.split("/")
            es_admin_proxy = DeviceProxy("dserver/hdbppes-srv/" + evt_subscriber[len(evt_subscriber)-1][-2:])
            print("Restarting device :{device_name}".format(device_name = es_admin_proxy))
            es_admin_proxy.RestartServer()
        time.sleep(4)
        

except (ConnectionFailed, CommunicationFailed, DevFailed) as exception:
    print("Exception occured: ", exception)

# TODO: For future use to remove sleep statement
# while True:
#     print("In While loop")
#     try:
#         if((str(conf_manager_proxy.State()) == "ON") and (str(evt_subscriber_proxy.State()) == "ON")):
#             print("State CM: ", str(conf_manager_proxy.State()))
#             print("State ES: ", str(evt_subscriber_proxy.State()))
#             break
#         else:
#             print("In else")
#             time.sleep(2)
#     except Exception as exc:
#         print("Exception: ", str(exc))
#         time.sleep(2)
#         continue
