"Script to configure hdbpp archiver"
# Imports
import sys
import getopt
import json
from time import sleep
import os
from tango import DeviceProxy, DevFailed, AttributeProxy


def cm_configure_attributes():
    """Method to configure an attribute."""
    attribute_configure_dict = {}
    attribute_configure_dict["configure_success_count"] = 0
    attribute_configure_dict["configure_fail_count"] = 0
    attribute_configure_dict["already_archived_count"] = 0
    attribute_configure_dict["total_attrib_count"] = 0
    attribute_configure_dict["attribute_started_count"] = 0
    attribute_configure_dict["error_starting_attrib_count"] = 0

    for cm in ARCHIVER_CONFIG:
        conf_manager = cm["confmanager"]
        configurations = cm["configuration"]
        for es in configurations:
            event_subscriber = es["eventsubscriber"]
            attribute_list = es["attributes"]
            polling_period = es["polling_period"]
            period_event = es["period_event"]
            attr_started_list = (
                evt_subscriber_proxy_dict[event_subscriber]
                .read_attribute("AttributeStartedList")
                .value
            )
            print("attr_started_list: ", str(attr_started_list))
            print("attribute_list: ", str(attribute_list))
            
            if attr_started_list is not None:
                for started_attr in attr_started_list:
                    for attr in attribute_list:
                        started_attr_in_list = started_attr.find(attr.lower())
                        if started_attr_in_list is not -1:
                            attribute_configure_dict["already_archived_count"] += 1
                            print("Attribute " + attr + " is already archived.")
                            attribute_list.remove(attr)
            print("Attributes to be configured: ", str(attribute_list))
            if attribute_list is not None:
                for attribute in attribute_list:
                    attribute_configure_dict["total_attrib_count"] += 1
                    attribute = (
                        "tango://"
                        + str(db_service)
                        + "."
                        + str(namespace)
                        + ".svc.cluster.local:10000/"
                        + attribute
                    )
                    print("Attribute is: ", str(attribute))
                    is_already_configured = False
                    attr_list = (
                        evt_subscriber_proxy_dict[event_subscriber]
                        .read_attribute("AttributeList")
                        .value
                    )
                    if attr_list is not None:
                        for already_archived in attr_list:
                            if attribute.lower() in str(already_archived).lower():
                                print("Attribute " + attribute + " already configured.")
                                is_already_configured = True
                                (
                                    attribute_configure_dict["attribute_started_count"],
                                    attribute_configure_dict["error_starting_attrib_count"],
                                ) = start_archiving(
                                    conf_manager_proxy_dict[conf_manager],
                                    attribute,
                                    attribute_configure_dict["attribute_started_count"],
                                    attribute_configure_dict["error_starting_attrib_count"],
                                )
                                break

                    if not is_already_configured:
                        print(
                            "Attribute "
                            + attribute
                            + " not configured. Configuring it now. "
                        )
                        max_retries = 5
                        sleep_time = 1
                        not_online = False
                        for loop_cnt in range(0, max_retries):
                            try:
                                att = AttributeProxy(attribute)
                                att.read()
                                break
                            except DevFailed as dev_failed:
                                if loop_cnt == (max_retries - 1):
                                    print(
                                        "Attribute "
                                        + attribute
                                        + " not online. Skipping it."
                                    )
                                    not_online = True
                                    break
                                print(
                                    "DevFailed exception: "
                                    + str(dev_failed.args[0].reason)
                                    + ". Sleeping for "
                                    + str(sleep_time)
                                    + "ss"
                                )
                                sleep(sleep_time)

                        if not_online:
                            continue

                        try:
                            conf_manager_proxy_dict[conf_manager].write_attribute(
                                "SetAttributeName", attribute
                            )
                            conf_manager_proxy_dict[conf_manager].write_attribute(
                                "SetArchiver", event_subscriber
                            )
                            conf_manager_proxy_dict[conf_manager].write_attribute(
                                "SetStrategy", "ALWAYS"
                            )
                            conf_manager_proxy_dict[conf_manager].write_attribute(
                                "SetPollingPeriod", int(polling_period)
                            )
                            conf_manager_proxy_dict[conf_manager].write_attribute(
                                "SetPeriodEvent", int(period_event)
                            )
                        except Exception as except_occured:
                            print(
                                "Exception while setting configuration manager arrtibutes: ",
                                except_occured,
                            )
                            attribute_configure_dict["configure_fail_count"] += 1
                            continue

                        try:
                            conf_manager_proxy_dict[conf_manager].AttributeAdd()
                            attribute_configure_dict["configure_success_count"] += 1
                            print("attribute " + attribute + " " + " added successfuly")

                        except DevFailed as dev_failed:
                            attribute_configure_dict["configure_fail_count"] += 1
                            print(
                                "Exception occured while adding attribute for archiving: ",
                                dev_failed,
                            )
            else:
                print("All the attributes from the list is already getting archived")
    return attribute_configure_dict
    

def start_archiving(
    conf_manager_proxy, str_attribute, attribute_started, error_starting_attrib
):
    """Method to start archiving an attribute."""
    try:
        conf_manager_proxy.command_inout("AttributeStart", str_attribute)
        attribute_started += 1
        print("Archiving for " + str_attribute + " has been started successfully.")
    except Exception as except_occured:
        error_starting_attrib += 1
        print("start_archiving except_occured: ", except_occured)

    return attribute_started, error_starting_attrib


# Main entrypoint of the script.
ATTR_LIST_FILE = ""
tango_host = ""
db_service = ""
namespace = ""
# parse arguments
try:
    opts, args = getopt.getopt(
        sys.argv[1:], "a:h:d:n", ["f=", "th=", "ds=", "ns="]
    )

except getopt.GetoptError:
    print("Please provide proper arguments.")
    print("Usage: $python configure_hdbpp.py --f=<filepath> --th=<TANGO_HOST> --ds=<DatabaseService> --ns=<NAMESPACE> OR")
    print("$python configure_hdbpp.py -a <filepath> -h <TANGO_HOST> -d <DATABASE_SERVICE> -n <NAMESPACE>")
    print("infile: File containing FQDNs of attributes to archive")
    print("th: TANGO_HOST of the device on which MVP is running")
    print("ds: Database service of the device on which MVP is running")
    print("ns: Namespace of the device on which MVP is running")
    sys.exit(2)

for opt, arg in opts:
    if opt in ("-a", "--f"):
        ATTR_LIST_FILE = arg
    elif opt in ("-h", "--th"):
        tango_host = arg
    elif opt in ("d", "--ds"):
        db_service = arg
    elif opt in ("n", "--ns"):
        namespace = arg

conf_manager_device_list = []
evt_subscriber_device_list = []
with open(ATTR_LIST_FILE, "r") as attrib_list_file:
    archiver_json = json.load(attrib_list_file)
    ARCHIVER_CONFIG = archiver_json["archiver"]
    for cm in ARCHIVER_CONFIG:
        conf_manager = cm["confmanager"]
        conf_manager_device_list.append(conf_manager)
        configurations = cm["configuration"]
        for es in configurations:
            event_subscriber = es["eventsubscriber"]
            evt_subscriber_device_list.append(event_subscriber)
            attribute_list = es["attributes"]
   
conf_manager_proxy_dict = {}
evt_subscriber_proxy_dict = {}
conf_manager_device_count = len(conf_manager_device_list)
evt_subscriber_device_count = len(evt_subscriber_device_list)
for conf_manager in conf_manager_device_list:
    conf_manager_proxy_dict[conf_manager] = DeviceProxy(tango_host + "/" + conf_manager)
for evt_subscriber in evt_subscriber_device_list:
    evt_subscriber_proxy_dict[evt_subscriber] = DeviceProxy(tango_host + "/" + evt_subscriber)

SLEEP_TIME = 6
MAX_RETRIES = 10
for x in range(0, MAX_RETRIES):
    try:
        attribute_configure_dict = cm_configure_attributes()
        print(
            "\nConfigured successfully:",
            attribute_configure_dict["configure_success_count"],
            ", Failed:",
            attribute_configure_dict["configure_fail_count"],
            ", Already archived:",
            attribute_configure_dict["already_archived_count"],
            ", Total attributes: ",
            attribute_configure_dict["total_attrib_count"],
            ", Attribute started:",
            attribute_configure_dict["attribute_started_count"],
            ", Error starting attribute:",
            attribute_configure_dict["error_starting_attrib_count"],
        )
        break
    except Exception:
        print("configure_attribute exception: " + str(sys.exc_info()))
        if x == (MAX_RETRIES - 1):
            sys.exit(-1)

    try:
        DEVICE_ADM_DICT = {}
        for conf_manager in conf_manager_device_list:
            conf_manager = conf_manager.split("/")
            DEVICE_ADM_DICT[ADM_VAL] = DeviceProxy("dserver/hdbppcm-srv/" + conf_manager[len(conf_manager)-1][-2:])
            DEVICE_ADM_DICT[ADM_VAL].RestartServer()
    except Exception:
        print("reset_conf_manager exception: " + str(sys.exc_info()[0]))

    sleep(SLEEP_TIME)

if attribute_configure_dict["configure_fail_count"] > 0:
    sys.exit(-1)

for evt_subscriber in evt_subscriber_proxy_dict:
    evt_subscriber_proxy_dict[evt_subscriber].Start()
