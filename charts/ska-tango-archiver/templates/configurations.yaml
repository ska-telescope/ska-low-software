{{ if .Values.enabled }}

{{ $default_tango_host := printf "%s-%s" "databaseds-tango-base-" .Release.Name }}
{{ $tango_host := tpl (coalesce .Values.global.tango_host .Values.tango_host $default_tango_host | toString) . }}
{{ $dsconfig := coalesce .Values.global.dsconfig .Values.dsconfig}}
{{ $itango := coalesce .Values.global.itango .Values.itango}}
{{- $telescope_slug := trimPrefix "SKA-" $.Values.telescope}}
---
apiVersion: v1
kind: ConfigMap
metadata:
  name: "attrconfig-configuration-{{ template "ska-tango-archiver.name" . }}-{{ .Release.Name }}"
  namespace: {{ .Release.Namespace }}
  labels:
{{ toYaml (coalesce .Values.global.labels .Values.labels "label:none") | indent 4 }}
  annotations:
{{ toYaml (coalesce .Values.global.annotations .Values.annotations "annotations:none") | indent 4 }}
data:
  configure_dbname_hostname.py:
{{ (tpl (.Files.Glob "data/configure_dbname_hostname.py").AsConfig . ) | indent 2 }}
  auto_configure.py:
{{ (tpl (.Files.Glob "data/auto_configure.py").AsConfig . ) | indent 2 }}
  configuration_file.json:
{{ (tpl (.Files.Glob "data/configuration_file.json").AsConfig . ) | indent 2 }}
  create_hdbpp_schema.sh:
{{ (tpl (.Files.Glob "data/create_hdbpp_schema.sh").AsConfig . ) | indent 2 }}
  hdb_schema.sql:
{{ (tpl (.Files.Glob "data/hdb_schema.sql").AsConfig . ) | indent 2 }}
---
apiVersion: batch/v1
kind: Job
metadata:
  name: create-hdbpp-schema-{{ template "ska-tango-archiver.name" . }}-{{ .Release.Name }}
  namespace: {{ .Release.Namespace }}
  labels:
{{ toYaml (coalesce .Values.global.labels .Values.labels "label:none") | indent 4 }}
  annotations:
{{ toYaml (coalesce .Values.global.annotations .Values.annotations "annotations:none") | indent 4 }}
spec:
  ttlSecondsAfterFinished: 300
  template:
    spec:
      volumes:
      - name: create-hdbpp-schema
        configMap:
          name: "attrconfig-configuration-{{ template "ska-tango-archiver.name" . }}-{{ .Release.Name }}"
      containers:
      - name: create-hdbpp-schema
        image: "{{ .Values.postgres.image.image }}:{{ .Values.postgres.image.tag }}"
        imagePullPolicy: {{ .Values.postgres.image.pullPolicy }}
        command:
          - sh
        args:
          - -c
          - "/bin/bash app/data/create_hdbpp_schema.sh {{ .Values.dbname }} {{ .Values.hostname }} {{ .Values.port }} {{ .Values.dbuser }} "
        env:
        - name: PGPASSWORD
          value: {{.Values.dbpassword}}
        - name: PGHOST
          value: {{coalesce .Values.hostname "timescaledb.ska-eda-mid-db.svc.cluster.local"}}
        - name: PGUSER
          value: {{coalesce .Values.hostname "admin"}}
        volumeMounts:
          - name: create-hdbpp-schema
            mountPath: /app/data
            readOnly: true
      restartPolicy: OnFailure
---
apiVersion: batch/v1
kind: Job
metadata:
  name: auto-configure-{{ template "ska-tango-archiver.name" . }}-{{ .Release.Name }}
  namespace: {{ .Release.Namespace }}
  labels:
    {{ toYaml (coalesce .Values.global.labels .Values.labels "label:none") | indent 4 }}
  annotations:
{{- if or (.Values.global.annotations) (.Values.annotations) }}
{{ toYaml (coalesce .Values.global.annotations .Values.annotations) | indent 4 }}
{{- end }}
spec:
  ttlSecondsAfterFinished: 300
  template:
    spec:
      volumes:
      - name: auto-configure
        configMap:
          name: "attrconfig-configuration-{{ template "ska-tango-archiver.name" . }}-{{ .Release.Name }}"
      initContainers:
        - name: wait-for-devices
          image: "{{ $dsconfig.image.registry }}/{{ $dsconfig.image.image }}:{{ $dsconfig.image.tag }}"
          imagePullPolicy: {{ $dsconfig.image.pullPolicy }}
          command:
            - sh
          args:
            - -c
{{- $configuration_managers := get .Values.deviceServers.archiver_cm $telescope_slug}}
{{- if $configuration_managers}}
{{- $configuration_manager := keys $configuration_managers | first}}
            - "retry --max=20 -- tango_admin --ping-device {{$configuration_manager}}
{{- end }}
{{- range $_, $device := .Values.deviceServers.archiver_es}}
{{- range (keys $device)}} && \
              retry --max=20 -- tango_admin --ping-device {{.}}
{{- end}}
{{- end}}"
          env:
          - name: TANGO_HOST
            value: {{ $tango_host }}
      containers:
      - name: auto-configure
        image: "{{ $itango.image.registry }}/{{ $itango.image.image }}:{{ $itango.image.tag }}"
        imagePullPolicy: {{ $itango.image.pullPolicy }}
        command:
          - sh
        args:   
          - -c             
          - 'pip install pyyaml requests ska-ser-logging ska-telmodel;
            /usr/bin/python3 data/auto_configure.py  "{{.Values.configuration_file_url}}" 
            "{{ .Values.car_sources }}"
            "{{ .Values.car_file_path }}"
            "{{.Release.Namespace}}"
            "{{.Values.global.cluster_domain }}"
            "{{$tango_host}}"'
        env:
        - name: TANGO_HOST
          value: {{ $tango_host }}
        volumeMounts:
          - name: auto-configure
            mountPath: /app/data
            readOnly: true
      restartPolicy: OnFailure
---
apiVersion: apps/v1
kind: Deployment
metadata:
  name: archwizard-{{ template "ska-tango-archiver.name" . }}-{{ .Release.Name }}
  namespace: {{ .Release.Namespace }}
  labels:
    app: archwizard
spec:
  replicas: 1
  selector:
    matchLabels:
      app: archwizard
  template:
    metadata:
      labels:
        app: archwizard
    spec:
      containers:
      - name: archwizard
        image: "{{ .Values.archiver_image.image.registry }}/{{ .Values.archiver_image.image.image }}:{{ .Values.archiver_image.image.tag }}"
        imagePullPolicy: {{ .Values.archiver_image.image.pullPolicy }}
        command:
          - "bash"
        args:
          - "-c"
          - "cd archwizard && uvicorn archwizard.main:app --host 0.0.0.0 --port 8000"
        ports:
            - containerPort: 8000
        env:
        - name: HDB_CONFIGURATION_MANAGERS
          value: {{.Values.archwizard_config }}

---
apiVersion: v1
kind: Service
metadata:
  name: archwizard
  namespace: {{ .Release.Namespace }}
  labels:
    app: archwizard
spec:
  ports:
  - port: 8000
    protocol: TCP
    targetPort: 8000
  selector:
    app: archwizard
  type: LoadBalancer
---
apiVersion: apps/v1
kind: Deployment
metadata:
  name: archviewer-{{ template "ska-tango-archiver.name" . }}-{{ .Release.Name }}
  namespace: {{ .Release.Namespace }}
  labels:
    app: archviewer
spec:
  replicas: 1
  selector:
    matchLabels:
      app: archviewer
  template:
    metadata:
      labels:
        app: archviewer
    spec:
      containers:
      - name: archviewer
        image: "{{ .Values.archiver_image.image.registry }}/{{ .Values.archiver_image.image.image }}:{{ .Values.archiver_image.image.tag }}"
        imagePullPolicy: {{ .Values.archiver_image.image.pullPolicy }}
        command:
          - "bash"
        args:
          - "-c"
          - "cd archviewer && uvicorn archviewer.app:app --host 0.0.0.0 --port 8082"
        env:
        {{- range $key, $value := .Values.archviewer.instances }}
        - name: timescale_host.{{ $value.name }}
          value: "{{ $value.timescale_host }}"
        - name: timescale_databases.{{ $value.name }}
          value: "{{ $value.timescale_databases }}"
        - name: timescale_login.{{ $value.name }}
          value: "{{ $value.timescale_login }}"
        {{- end }}
        ports:
            - containerPort: 8082
---
apiVersion: v1
kind: Service
metadata:
  name: archviewer
  namespace: {{ .Release.Namespace }}
  labels:
    app: archviewer
spec:
  ports:
  - port: 8082
    protocol: TCP
    targetPort: 8082
  selector:
    app: archviewer
  type: LoadBalancer
---
apiVersion: apps/v1
kind: Deployment
metadata:
  name: configurator-{{ template "ska-tango-archiver.name" . }}-{{ .Release.Name }}
  namespace: {{ .Release.Namespace }}
  labels:
    app: configurator
spec:
  replicas: 1
  selector:
    matchLabels:
      app: configurator
  template:
    metadata:
      labels:
        app: configurator
    spec:
      containers:
      - name: configurator
        image: "{{ .Values.archiver_image.image.registry }}/{{ .Values.archiver_image.image.image }}:{{ .Values.archiver_image.image.tag }}"
        imagePullPolicy: {{ .Values.archiver_image.image.pullPolicy }}
        command:
          - "bash"
        args:
          - "-c"
          - "cd src/ska_eda_configurator && uvicorn eda_configurator:app --host 0.0.0.0 --port 8003"
        ports:
            - containerPort: 8003
        env:
        - name: TANGO_HOST
          value: {{$tango_host}}
        - name: TELESCOPE_ENVIRONMENT
          value: {{.Values.telescope_environment}}
        - name: KUBE_NAMESPACE
          value: {{.Release.Namespace}}
---
apiVersion: v1
kind: Service
metadata:
  name: configurator
  namespace: {{ .Release.Namespace }}
  labels:
    app: configurator
spec:
  ports:
  - port: 8003
    protocol: TCP
    targetPort: 8003
  selector:
    app: configurator
  type: LoadBalancer
---
apiVersion: networking.k8s.io/v1
kind: Ingress
metadata:
  name: configurator-ingress-{{ template "ska-tango-archiver.name" . }}-{{ .Release.Name }}
  namespace: {{ .Release.Namespace }}
  annotations:
    nginx.ingress.kubernetes.io/rewrite-target: /$1
spec:
  ingressClassName: "nginx"
  rules:
    - http:
        paths:
          - path: /{{ .Release.Namespace }}/configurator/(.*)
            pathType: Prefix
            backend:
              service:
                name: configurator
                port:
                  number: 8003
{{ end }}
