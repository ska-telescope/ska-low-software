ska-low-software
================

This project provides the application software layer
for the SKA-Low telescope of the `Square Kilometre Array`_.

.. _Square Kilometre Array: https://skatelescope.org/


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
