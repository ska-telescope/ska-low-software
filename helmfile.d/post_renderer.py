#!/usr/bin/env python
"""
A place to put last-resort hacks to K8s resource for things that we need but
aren't yet supported by product charts. Everything in here should have
associated work in progress to move the functionality upstream.

To run this, you need to install the `ruamel.yaml` package in your Python env.
"""
import sys
import re
from functools import partial

# preserves formatting way better than PyYAML
from ruamel.yaml import YAML

PATCHES = [
    # allow privileged on MccsTile Pods so that we can get core dumps for SKB-609
    {
        "match": {
            ("kind",): "StatefulSet",
            ("metadata", "name"): re.compile(r"(tpms-tile-|spsstations-spsstation-)"),
        },
        "patch": {
            ("spec", "template", "spec", "containers", 0, "securityContext", "privileged"): True,
        },
    },
]


yaml = YAML()
yaml.width = 4096
yaml.preserve_quotes = True
yaml.allow_duplicate_keys = True  # gremlin in ska-pst which generates multiple app: labels

yamls = list(
    doc  # one for each k8s resource in Helm output
    for doc in yaml.load_all(sys.stdin)
    if doc is not None  # null docs don't dump properly
)


def get_in(d, keys, default=None):
    """Gets a value in a nested structure of dicts/lists, or a default if not found."""
    try:
        for key in keys:
            d = d[key]
        return d
    except (KeyError, IndexError, TypeError):
        return default


def set_in(d, keys, value):
    """Sets a value in a nested structure of dicts/lists."""
    *path_keys, last_key = keys
    for key in path_keys:
        try:
            d.__getitem__(key)
        except (KeyError, IndexError):
            d[key] = dict()
        d = d[key]
    d[last_key] = value


def make_pred(condition):
    """
    Condition may be specified as
    - a re.Pattern, in which case the value is passed to match()
    - a callable, in which case it's called
    - any other value, in which case it's compared by equality
    """
    if isinstance(condition, re.Pattern):
        return partial(condition.match)
    elif callable(condition):
        return condition
    else:
        return lambda x: condition == x


for doc in filter(None, yamls):
    for match_patch in PATCHES:
        matches = match_patch["match"]
        patches = match_patch["patch"]
        if all(
                make_pred(val)(get_in(doc, keys))
                for keys, val in matches.items()
        ):
            for keys, val in patches.items():
                set_in(doc, keys, val)


yaml.dump_all(yamls, sys.stdout)
