#!/usr/bin/env bash

watch_path="$1"
shift 1

inotifywait --recursive --event close_write,moved_to,create --include '\.yaml$' --monitor "$watch_path" |
while read -r directory events filename; do
  (set -x; yaml2archiving "$@" "$directory$filename");
done
